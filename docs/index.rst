.. Vacuity documentation master file, created by
   sphinx-quickstart on Fri Dec 20 08:52:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Vacuity's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   available-clients
   vacuity.client
   vacuity



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
