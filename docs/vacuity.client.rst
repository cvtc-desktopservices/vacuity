Client abstraction
==================

Implement these abstractions to allow your data source to be displayed in Vacuity.

See :doc:`available-clients` for information on clients that are available from the current Vacuity codebase, and how to use them.

Client modules should **never** carry state unless it is saved in an external place. A separate module state could be created for each thread or worker in a WSGI server.

vacuity.client.abstract module
------------------------------

.. autoclass:: vacuity.client.abstract.Complex
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.Building
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.ComputerLab
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.LabComputerAvailability
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.AvailabilitySummary
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.Room
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.RoomAvailability
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.VacuityException
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.BuildingNotFoundError
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: vacuity.client.abstract.RoomNotFoundError
   :members:
   :undoc-members:
   :show-inheritance:
