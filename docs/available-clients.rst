Available clients
=================

This page lists built-in Vacuity clients. These can be used to connect to a room schedule and lab availability database.

Clients are instantiated in your ``run`` file. For example, this file instantiates and runs using the Mock client::

    from flask import Flask

    import vacuity.controllers
    from vacuity.client import mock

    vacuity.controllers.COMPLEX = mock.MOCKED_COMPLEX

    # Static folder is disabled as long as we only have one blueprint
    # https://stackoverflow.com/a/25804585
    app = Flask(__name__, static_folder=None)

    # Clean up Jinja output
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    app.register_blueprint(vacuity.controllers.main, url_prefix="/")

When saved as ``run.py` and run with ``gunicorn run:app``, this app loads the mocked complex defined in the mock module. The useful line is ``vacuity.controllers.COMPLEX = ...``, which sets the complex used in the thread.

vacuity.client.cvtc module
--------------------------

.. automodule:: vacuity.client.cvtc
   :members:
   :undoc-members:
   :show-inheritance:

vacuity.client.mock module
--------------------------

.. automodule:: vacuity.client.mock
   :members:
   :undoc-members:
   :show-inheritance:
