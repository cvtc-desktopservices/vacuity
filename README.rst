Sidereal Vacuity
================

**Heads up!**

Vacuity is currently unmaintained

.. epigraph::

   vacuity, va-ˈkyü-ə-tē:

   An empty space

   -- Merriam-Webster

Vacuity helps people find empty space on campus.

.. image:: example/vacuity-tablet.png

.. image:: example/vacuity-phone.png


Run Vacuity
-----------

Vacuity can be run using one of the 'run' scripts in this folder. The default, ``run.py``, requires only the dependencies installed with Vacuity::

    pipenv install
    pipenv run python run.py

This will start a development server on your local machine.

With Gunicorn installed, Vacuity's mock module can be run with a command similar to::

    gunicorn --bind=127.0.0.1:8000 --worker-class=gthread run:app

This will provide a server which is, in theory, faster for most workloads that Vacuity clients will handle. The ``gthread`` worker should be able to handle more requests at a time for IO-bound tasks, such as querying backend APIs via HTTP.

Vacuity can run with any WSGI, we recommend Gunicorn. You can find more information on deploying a Gunicorn application here: https://docs.gunicorn.org/en/stable/deploy.html

CVTC client
^^^^^^^^^^^

You will additionally need to set some environment variables in order to use the CVTC Vacuity client. See the top of the `run_cvtc.py <run_cvtc.py>`_ file for a full list. Then, replace ``run:app`` with ``run_cvtc:app`` in your Gunicorn command.

To learn more about setting up the LabStats datasource for use with the CVTC Vacuity client, see the top of the `vacuity/client/cvtc.py <vacuity/client/cvtc.py>`_ file.

Develop Vacuity
---------------

Vacuity's internal documentation can be found at https://vacuity.readthedocs.io/. Here you'll find information on the abstractions used to implement client modules.

We develop Vacuity using Python 3.6.

Please run tests prior to committing your work. You can run tests with the following command, provided you've got your environment ready with ``pipenv install --dev``::

    pipenv run pytest --pylint --pylint-rcfile=.pylintrc

The codebase is formatted using `Black <https://github.com/psf/black>`_. Please run it before you commit.
