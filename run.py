"""Runs Vacuity with the mock client class"""

from flask import Flask

import vacuity.controllers
from vacuity.client import mock

vacuity.controllers.COMPLEX = mock.MOCKED_COMPLEX

# Static folder is disabled as long as we only have one blueprint
# https://stackoverflow.com/a/25804585
app = Flask(__name__, static_folder=None)

# Clean up Jinja output
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True

app.register_blueprint(vacuity.controllers.main, url_prefix="/")

if __name__ == "__main__":
    app.run()
