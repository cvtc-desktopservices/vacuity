"""Client implementation with pre-made test and demonstration data"""
# pylint: disable=missing-class-docstring

from collections import namedtuple
from datetime import datetime, time
from typing import List

import arrow

from . import abstract

TIMEZONE = "America/Chicago"


MockUnavailableBlock = namedtuple("MockUnavailableBlock", ("start", "end"))


class MockComputerLab(abstract.ComputerLab):
    def __init__(
        self, total_computers: int, available_computers: int, software: List[str]
    ):
        if total_computers > 0:
            self._availability = MockLabComputerAvailability(
                available_computers, total_computers
            )
        else:
            self._availability = None
        self._software = software

    @property
    def computer_availability_now(self):
        return self._availability

    @property
    def installed_software_names(self):
        return self._software


class MockLabComputerAvailability(abstract.LabComputerAvailability):
    def __init__(self, available, total):
        self._available = available
        self._total = total

    @property
    def available(self):
        return self._available

    @property
    def total(self):
        return self._total

    @property
    def summary(self):
        percent_available = self.available / self.total
        if percent_available > 0.25:
            return abstract.AvailabilitySummary.GOOD
        if percent_available > 0:
            return abstract.AvailabilitySummary.POOR

        return abstract.AvailabilitySummary.NONE


MockRoomAvailability = namedtuple(
    "MockRoomAvailability", field_names=["moment", "summary", "humanized"]
)


class MockRoom(abstract.Room):
    def __init__(
        self,
        identifier: str,
        code: str,
        unavailability: List[MockUnavailableBlock],
        lab: abstract.ComputerLab = None,
    ):
        self._id = identifier
        self._code = code
        self._unavailability = unavailability
        self._lab = lab

    @property
    def identifier(self):
        return self._id

    @property
    def code(self):
        return self._code

    @property
    def availability_now(self):
        now = arrow.now(TIMEZONE)

        unavailable_now = self._unavailability_at(now)
        if unavailable_now is not None:
            return MockRoomAvailability(
                now,
                abstract.AvailabilitySummary.NONE,
                "Unavailable until " + unavailable_now.end.format("h:mm A"),
            )

        next_unavailable = self._next_unavailability_today_after(now)
        if next_unavailable is not None:
            return MockRoomAvailability(
                now,
                abstract.AvailabilitySummary.POOR,
                "Available until " + next_unavailable.start.format("hA"),
            )

        return MockRoomAvailability(
            now, abstract.AvailabilitySummary.GOOD, "Available all day"
        )

    @property
    def lab(self):
        return self._lab

    def _unavailability_at(self, reference) -> MockUnavailableBlock:
        """The block of unavailability that occurs during reference.

        If we find an unavialability block which intercepts the point of
        reference, it is returned. Otherwise, None is returned.
        """

        for block in self._unavailability:
            if block.start < reference < block.end:
                return block

        return None

    def _next_unavailability_today_after(self, reference) -> MockUnavailableBlock:
        """The next block that the room becomes unavailable after reference.

        If we find an unavailability block after the point of reference, it is
        returned. Otherwise, None is returned.
        """

        for block in self._unavailability:
            if reference < block.start:
                return block

        return None


class MockBuilding(abstract.Building):
    def __init__(
        self, identifier: str, abbreviation: str, description: str, rooms: dict
    ):
        self._id = identifier
        self._abbr = abbreviation
        self._desc = description
        self._rooms = rooms

    @property
    def abbreviation(self):
        return self._abbr

    @property
    def description(self):
        return self._desc

    @property
    def identifier(self):
        return self._id

    @property
    def rooms(self):
        return self._rooms.values()

    def room_for_identifier(self, identifier):
        try:
            return self._rooms[identifier]
        except KeyError:
            raise abstract.RoomNotFoundError("Desired identifier not found")


class MockComplex(abstract.Complex):
    def __init__(self, buildings: dict):
        self._buildings = buildings

    def building_for_identifier(self, identifier):
        try:
            return self._buildings[identifier]
        except KeyError:
            raise abstract.BuildingNotFoundError

    @property
    def buildings(self):
        return self._buildings.values()


TODAY_MIDNIGHT = arrow.get(datetime.combine(datetime.today(), time.min), TIMEZONE)


def midnight_shifted_by(hours, minutes):
    """Utility to shift today (at module import time) by hours and minutes"""
    return TODAY_MIDNIGHT.shift(hours=hours, minutes=minutes)


ROOM_DSI_101 = MockRoom(
    "101",
    "101",
    [
        MockUnavailableBlock(midnight_shifted_by(9, 0), midnight_shifted_by(10, 55)),
        MockUnavailableBlock(midnight_shifted_by(11, 30), midnight_shifted_by(12, 55)),
        MockUnavailableBlock(midnight_shifted_by(16, 0), midnight_shifted_by(17, 55)),
    ],
)

ROOM_BIO_002 = MockRoom(
    "002", "002", [], lab=MockComputerLab(10, 3, ["Super Cool Software"])
)

ROOM_BIO_003 = MockRoom(
    "003", "003", [], lab=MockComputerLab(4, 2, ["Super Cool Software"])
)

ROOM_BIO_102 = MockRoom(
    "102",
    "102",
    [MockUnavailableBlock(midnight_shifted_by(11, 20), midnight_shifted_by(15, 0))],
    lab=MockComputerLab(10, 2, ["Super Cool Software"]),
)

ROOM_BIO_103 = MockRoom(
    "103",
    "103",
    [MockUnavailableBlock(midnight_shifted_by(15, 0), midnight_shifted_by(15, 55))],
    lab=MockComputerLab(15, 0, ["Super Cool Software"]),
)

ROOM_BIO_104 = MockRoom(
    "104", "Open Lab", [], lab=MockComputerLab(20, 15, ["Super Cool Software"])
)

ROOM_ADM_SIM = MockRoom(
    "SIM",
    "Simulation HQ",
    [MockUnavailableBlock(midnight_shifted_by(8, 0), midnight_shifted_by(11, 55))],
)

ROOM_SEC_NOSLP = MockRoom(
    "NOSLP",
    "No Sleep Allowed",
    [MockUnavailableBlock(midnight_shifted_by(4, 0), midnight_shifted_by(11, 55))],
)

BLDG_DSI = MockBuilding(
    "DSI", "DSI", "Data and Applied Science Center", {"101": ROOM_DSI_101}
)
BLDG_BIO = MockBuilding(
    "BIO",
    "BIO",
    "Biology and Life Center",
    {
        "002": ROOM_BIO_002,
        "003": ROOM_BIO_003,
        "102": ROOM_BIO_102,
        "103": ROOM_BIO_103,
        "104": ROOM_BIO_104,
    },
)
BLDG_ADM = MockBuilding("ADM", "ADM", "Adams Center", {"SIM": ROOM_ADM_SIM})
BLDG_SEC = MockBuilding(
    "SEC", "SEC", "Security Services Center", {"NOSLP": ROOM_SEC_NOSLP}
)

MOCKED_COMPLEX = MockComplex(
    {"DSI": BLDG_DSI, "BIO": BLDG_BIO, "ADM": BLDG_ADM, "SEC": BLDG_SEC}
)
