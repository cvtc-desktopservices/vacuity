"""Interfaces for creating building and room clients"""

import datetime
from abc import ABC, abstractmethod
from enum import Enum
from typing import List


class AvailabilitySummary(Enum):
    """Represents momentary one-word of availability of a Vacuity resource.

    GOOD means the resource is plentiful.

    POOR means the resource is not very available.

    NONE means the resource is unavailable (or very close to being
    unavailable, depending on your needs).

    ERROR means Vacuity could not determine the state of the resource.
    """

    GOOD = "good"
    POOR = "poor"
    NONE = "none"
    ERROR = "error"


class LabComputerAvailability(ABC):
    """
    A point-in-time state of the available or unavailable computers in a lab.
    """

    @property
    @abstractmethod
    def available(self) -> int:
        """The number of computers that are available for use."""

    @property
    @abstractmethod
    def total(self) -> int:
        """The number of computers that the room contains."""

    @property
    @abstractmethod
    def summary(self) -> AvailabilitySummary:
        """A single-word summary of the availability of computers in the room.

        .. seealso::

            :class:`LabComputerAvailabilitySummary`
        """


class ComputerLab(ABC):
    """
    A group of computers in a room with a common set of installed software.
    """

    @property
    @abstractmethod
    def computer_availability_now(self) -> LabComputerAvailability:
        """
        A :class:`LabComputerAvailability` representing the lab's current
        state.
        """

    @property
    @abstractmethod
    def installed_software_names(self) -> List[str]:
        """
        Human-readable names of interesting software installed in this lab.

        "Interesting" depends on your institution. All of the software
        installed on the machine may be interesting to you, or only a few
        things.
        """


class RoomAvailability(ABC):
    """Describes a room's availability status at a point in time"""

    @property
    @abstractmethod
    def moment(self) -> datetime.datetime:
        """The moment in time that this unavailable status represents"""

    @property
    @abstractmethod
    def summary(self) -> AvailabilitySummary:
        """Whether the room is available or not.

        .. seealso::

            :class:`RoomAvailabilitySummary`
        """

    @property
    @abstractmethod
    def humanized(self) -> str:
        """
        A human-readable description of when the room will become available or
        unavailable.

        For example, "available for four hours", "available until 4PM", or
        "unavailable until 3:55 PM". The format is up to you, but try to keep
        it between three to five words.
        """


class Room(ABC):
    """Describes a room.

    A room may be scheduled, and it may contain computers, of which some are
    available and others are in use.
    """

    @property
    @abstractmethod
    def identifier(self) -> str:
        """The meaningful identifier of this room.

        Used to represent this room when requesting information about it from
        the backend.
    """

    @property
    @abstractmethod
    def code(self) -> str:
        """The human-readable name for the room.

        May be a number (e.g. 104), a name (e.g. Bailey), or even the same as
        the identifier.
        """

    @property
    @abstractmethod
    def availability_now(self) -> RoomAvailability:
        """
        A :class:`RoomAvailability` for the room's status at access time.

        Of course, like any value, you may choose to cache results so "now"
        may be "15 minutes ago", just as long as the data is up-to-date enough
        for your users.
        """

    @property
    @abstractmethod
    def lab(self) -> ComputerLab:
        """
        A :class:`LabInformation` representing the computer lab this room
        contains, or None if the room does not contain a computer lab.
        """


class Building(ABC):
    """Describes a building.

    A building contains :class:`Room`
    """

    @property
    @abstractmethod
    def identifier(self) -> str:
        """The meaningful identifier of this building.

        Used to represent this building when requesting information about it from
        the backend.
        """

    @property
    @abstractmethod
    def abbreviation(self) -> str:
        """A shorter name for the building, one to five characters long ideally.

        Used to denote the building alongside room codes or anywhere else that it
        is not possible to display the building description.
        """

    @property
    @abstractmethod
    def description(self) -> str:
        """A long, human-recognizable name for the building.

        Used whenever possible when the user needs to select a building. For
        example, a list of building descriptions is displayed when asking the user
        to filter by building.
        """

    @property
    @abstractmethod
    def rooms(self) -> List[Room]:
        """Returns a list of Room that this building contains."""

    @abstractmethod
    def room_for_identifier(self, identifier) -> Room:
        """Returns the Room with the specified identifier.

        :raises RoomNotFoundError: The requested identifier does not
            correspond to a room we know about.
        """


class Complex(ABC):
    """Manages initial access to :class:`Building`, allowing filtering."""

    @property
    @abstractmethod
    def buildings(self) -> List[Building]:
        """Returns all of the Buildings we know about."""

    @abstractmethod
    def building_for_identifier(self, identifier) -> Building:
        """Returns the Building with the specified identifier.

        :raises BuildingNotFoundError: The requested identifier does not
            correspond to a building we know about.
        """

    @property
    def vacuity_feedback_url(self) -> str:
        """Returns an HTML-safe feedback URL.

        This URL will be placed in a "Give Feedback" link near the bottom of
        every page.

        If you do not provide a feedback URL, the "Give Feedback" link will
        not be shown.
        """

        return None


class VacuityException(Exception):
    """Base class for errors arising from Vacuity"""


class BuildingNotFoundError(VacuityException):
    """Thrown when a Building could not be found in a Complex"""


class RoomNotFoundError(VacuityException):
    """Thrown when a Room could not be found in a Building"""
