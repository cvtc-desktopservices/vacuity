"""Client for CVTC room schedule data API and LabStats lab usage API

This client retrieves its list of rooms from LabStats. Each top-level group in
LabStats is considered a building in the Complex. Every group underneath a
root group is considered a candidate for display in Vacuity. Candidates are
selected for pre-filtering if they match these criteria:

* Group contains stations, not groups
* Group has a selection criteria, EITHER:
    * Group has "lab features enabled"
    * Group's description contains the string "vacuity_enable"

Once a group meets these criteria, its lab and room schedule information will
be displayed in Vacuity. To prevent lab usage information from being
displayed, add "vacuity_nolab" to the group's description.
"""

from collections import namedtuple
from datetime import datetime
from os import environ
from typing import List

import requests

from chronology.events import NaiveRecurringEvent
from chronology.notation import days_from_fields, time_from_24h_notation
from labstats_api import LabStats
from labstats_api.models import Group

from . import abstract

BUILDING_IDS = {
    "CFCMPS": "CFC",
    "EC BUS": "BEC",
    "EC MFG": "ECM",
    "EC NIC": "ATC",
    "ECDIES": "DEC",
    "ECEANX": "EA",
    "ECEMER": "ES",
    "ECENER": "EEC",
    "ECHLTH": "HEC",
    "ECWANX": "WA",
    "MECMPS": "MEC",
    "NLVCNT": "NEC",
    "RFCMPS": "RFC",
}

BUILDING_DESCRIPTIONS = {
    "CFCMPS": "Chippewa Falls Campus",
    "EC BUS": "Business Education Center",
    "EC MFG": "Manufacturing Education Center",
    "EC NIC": "Applied Technology Center",
    "ECDIES": "Diesel Education Center",
    "ECEANX": "East Annex",
    "ECEMER": "Emergency Services Education Center",
    "ECENER": "Energy Education Center",
    "ECHLTH": "Health Education Center",
    "ECWANX": "West Annex",
    "MECMPS": "Menomonie Campus",
    "NLVCNT": "Neillsville Campus",
    "RFCMPS": "River Falls Campus",
}

UPSTREAM_DATA_API = environ.get("UPSTREAM_DATA_API", None)
FEEDBACK_URL = environ.get("FEEDBACK_URL", None)


def get_group_in_list_of_groups(group_name, group_list):
    """Returns the group with name matching group_name in group_list.

    If no match is found, ValueError is raised.
    """

    for group in group_list:
        if group.name == group_name:
            return group

    raise ValueError("Group with name group_name not found in group_list")


def event_json_to_naive_events(event_json):
    """
    Converts a very specific JSON-formatted list of objects to Chronology
    NaiveEvents.
    """
    events = []
    for entry in event_json:

        start_time = time_from_24h_notation(entry["BEGIN_TIME"])
        end_time = time_from_24h_notation(entry["END_TIME"])

        start_date = datetime.strptime(entry["START_DATE"], "%Y-%m-%dT%H:%M:%SZ")
        end_date = datetime.strptime(entry["END_DATE"], "%Y-%m-%dT%H:%M:%SZ")

        days = days_from_fields(
            entry["MON"],
            entry["TUE"],
            entry["WED"],
            entry["THU"],
            entry["FRI"],
            entry["SAT"],
            entry["SUN"],
        )

        events.append(
            NaiveRecurringEvent(start_date, end_date, start_time, end_time, days)
        )
    return events


class CVTCComplex(abstract.Complex):
    """Uses hard-coded data to return CVTC buildings

    :param labstats_api_url: URL of hosted LabStats instance's API.

    :param labstats_api_key: API key used to get data from LabStats.

    :param session: A Requests Session object. It is recommended to use
        CacheControl or a similar library to add caching, else you will hit
        API rate limiting.
    """

    def __init__(self, labstats_api_url, labstats_api_key, session: requests.Session):
        # LabStats doesn't send Cache-Control headers, so we must use our own
        # intuition. 1 minute seems long enough to avoid rate limiting, even
        # with a lot of workers. Of course the number of workers must go
        # down depending on the number of rooms we want to serve on a single
        # page when under load.
        self._session = session
        self._labstats = LabStats(
            labstats_api_url, labstats_api_key, session=self._session
        )

    @classmethod
    def formal_id_for_banner_id(cls, banner_id):
        """Given the Banner ID of a CVTC building, get the ID used elsewhere.

        For example, the Business Education Center is "EC BUS" in Banner but
        "BEC" elsewhere.
        """

        return BUILDING_IDS[banner_id]

    @property
    def _root_groups(self) -> List[Group]:
        """Returns the groups at the root of the LabStats account.

        Our LabStats configuration has all of our buildings as the root
        groups, all of the rooms of the buildings are directly below. In
        order to find the buildings faster, we need only the groups with
        no parent (the root ones).
        """
        ls_groups = self._labstats.get_groups(contents="groups")
        root_groups = [group for group in ls_groups if group.parent_group_id is None]
        return root_groups

    def building_for_identifier(self, identifier):
        try:
            formal_id = self.formal_id_for_banner_id(identifier)
        except KeyError:
            raise abstract.BuildingNotFoundError("Building not found")
        ls_building = get_group_in_list_of_groups(formal_id, self._root_groups)
        return CVTCBuilding(
            identifier, labstats_group=ls_building, session=self._session
        )

    @property
    def buildings(self):
        buildings = []
        root_groups = self._root_groups
        for (banner_id, formal_id) in BUILDING_IDS.items():
            ls_building = get_group_in_list_of_groups(formal_id, root_groups)
            buildings.append(
                CVTCBuilding(
                    banner_id, labstats_group=ls_building, session=self._session
                )
            )

        return buildings

    @property
    def vacuity_feedback_url(self) -> str:
        return FEEDBACK_URL


class CVTCBuilding(abstract.Building):
    """
    :param identifier: The Banner ID of this building

    :param labstats_group: An optional LabStats group representing this
        building. Searches for room groups will occur within this group.
    """

    def __init__(
        self, identifier, labstats_group: Group = None, session: requests.Session = None
    ):
        self._identifier = identifier
        self._labstats_group = labstats_group
        self._session = session or requests.Session()

    def _get_rooms(self):
        ls_groups = self._labstats_group.get_child_groups()
        rooms = {}

        for group in ls_groups:
            # Groups that we want to display have lab features enabled or have
            # "vacuity_enable" in their description
            if group.contents != "stations" or not (
                group.has_lab_features_enabled
                or (group.description and "vacuity_enable" in group.description)
            ):
                continue
            room_name = group.name.split(" ", 1)[1]

            # If the group description contains "vacuity_nolab", we should not
            # add the LabStats Group object so it doesn't show lab data
            if group.description and "vacuity_nolab" in group.description:
                group = None

            rooms[room_name] = CVTCRoom(
                room_name,
                self.identifier,
                self.abbreviation,
                labstats_group=group,
                session=self._session,
            )

        return rooms

    @property
    def identifier(self):
        return self._identifier

    @property
    def abbreviation(self):
        return BUILDING_IDS[self._identifier]

    @property
    def description(self):
        return BUILDING_DESCRIPTIONS[self._identifier]

    def room_for_identifier(self, identifier):
        try:
            return self._get_rooms()[identifier]
        except KeyError:
            raise abstract.RoomNotFoundError("Desired identifier not found")

    @property
    def rooms(self):
        return self._get_rooms().values()


CVTCRoomAvailability = namedtuple(
    "CVTCRoomAvailability", field_names=["moment", "summary", "humanized"]
)


class CVTCRoom(abstract.Room):
    """A Room capable of combining data from LabStats and Banner.

    :param identifier: This room's meaningful ID.

    :param banner_building_id: The name of the building this room belongs to
        in banner.

    :param formal_building_id: The name of the building that this room belongs
        to, according to the "rest of the world".

    :param labstats_group: The LabStats group corresponding to this room.
        If provided, this Room is capable of returning computer lab usage
        data. If not provided, any requests for computer lab usage data will
        return None.
    """

    def __init__(
        self,
        identifier,
        banner_building_id,
        formal_building_id,
        labstats_group: Group = None,
        session: requests.Session = None,
    ):  # pylint: disable=too-many-arguments
        self._identifier = identifier
        self._banner_building_id = banner_building_id
        self._formal_building_id = formal_building_id
        self._session = session or requests.Session()
        if isinstance(labstats_group, Group):
            self._labstats_group = labstats_group
        else:
            self._labstats_group = None

    @property
    def identifier(self):
        return self._identifier

    @property
    def code(self):
        return self._identifier

    @property
    def _unavailability_today(self) -> NaiveRecurringEvent:
        """
        All of the blocks that this room is unavailable today, sorted by the
        time they start
        """
        if not UPSTREAM_DATA_API:
            return None

        schedule_request = self._session.get(
            UPSTREAM_DATA_API.format(
                campus=self._banner_building_id, room_number=self.identifier
            ),
            timeout=10,
        )
        schedule_request.raise_for_status()

        schedule_data = schedule_request.json()

        if not schedule_data:
            return []

        today_in_iso = datetime.now().date().isoweekday()

        possible_events = [
            event
            for event in event_json_to_naive_events(schedule_data)
            if today_in_iso in event.days
        ]

        return sorted(possible_events, key=lambda event: event.start_time)

    @property
    def _next_event(self) -> NaiveRecurringEvent:
        """Returns the next event in the room after now"""

        now = datetime.now().time()
        # _unavailability_today is sorted by start_time, so this works
        for event in self._unavailability_today:
            if now < event.start_time or now < event.end_time:
                return event

        return None

    @property
    def availability_now(self):
        now_datetime = datetime.now()
        now = now_datetime.time()
        unavailable = self._next_event

        if not unavailable:
            return CVTCRoomAvailability(
                now_datetime, abstract.AvailabilitySummary.GOOD, "Available all day"
            )

        if unavailable.start_time < now < unavailable.end_time:
            return CVTCRoomAvailability(
                now_datetime,
                abstract.AvailabilitySummary.NONE,
                "Unavailable until "
                + "{d:%l}:{d.minute:02} {d:%p}".format(d=unavailable.end_time),
            )

        if now < unavailable.start_time:
            return CVTCRoomAvailability(
                now_datetime,
                abstract.AvailabilitySummary.POOR,
                "Available until "
                + "{d:%l}:{d.minute:02} {d:%p}".format(d=unavailable.start_time),
            )

        return CVTCRoomAvailability(
            now_datetime, abstract.AvailabilitySummary.GOOD, "Available all day"
        )

    @property
    def lab(self):
        if self._labstats_group is not None:
            return CVTCLab(self._labstats_group)

        return None


class CVTCLab(abstract.ComputerLab):
    """A Lab which pulls data from LabStats.

    :param labstats_group: Group corresponding to this Lab.
    """

    def __init__(self, labstats_group: Group):

        if not isinstance(labstats_group, Group):
            raise TypeError("CVTCLab requires a labstats_api.models.Group")

        self._labstats_group = labstats_group

    @property
    def computer_availability_now(self):
        status = self._labstats_group.get_status()
        available = status.offline + status.powered_on
        total = status.offline + status.powered_on + status.in_use

        if total == 0:
            return None
        return CVTCLabComputerAvailability(available, total)

    @property
    def installed_software_names(self):

        # MMs can have different software than user stations, so we'll try
        # to avoid checking the MM if possible
        stations = self._labstats_group.get_stations(limit=2)

        try:
            mm_computer = stations[0]
        except IndexError:
            return None

        if mm_computer.name.casefold().endswith("mm"):
            station = stations[0]
        else:
            try:
                station = stations[1]
            except IndexError:
                station = stations[0]

        return [app.name for app in station.get_apps() if app.is_tracked]


class CVTCLabComputerAvailability(abstract.LabComputerAvailability):
    """Point-in-time state of computers available in a lab at CVTC."""

    def __init__(self, available, total):
        self._available = available
        self._total = total

    @property
    def available(self):
        return self._available

    @property
    def total(self):
        return self._total

    @property
    def summary(self):
        if self.available <= 1:
            # Comparing to 1 PC ensures that if the teaching station is the
            # only available station, we say there is no availability.
            # This also prevents disappointment from a single machine being
            # taken between someone checking the status and actually going to
            # the lab.
            return abstract.AvailabilitySummary.NONE

        availability_percentage = self.available / self.total

        if availability_percentage < 0.25:
            return abstract.AvailabilitySummary.POOR
        return abstract.AvailabilitySummary.GOOD
