"""Uses client abstractions to serve Vacuity webpages.

Set the COMPLEX module variable prior to calling any functions in this module.
"""
import urllib.parse
from datetime import datetime
from typing import List

from flask import Blueprint, abort, redirect, render_template, request

from .client.abstract import (
    AvailabilitySummary,
    BuildingNotFoundError,
    Complex,
    Room,
    RoomNotFoundError,
)

COMPLEX = None

main = Blueprint("main", "vacuity", template_folder="templates", static_folder="static")


def _icon_for_availability_summary(summary: AvailabilitySummary) -> (str, str):
    """A fontawesome icon name and color for the given room summary.

    Vacuity makes use of FontAwesome icons for at-a-glance status of
    resources. This function returns an appropriate CSS class value for
    one of these icons, such as ``"fas fa-check-circle text-success"``. This
    can be placed inside of an ``<i>`` tag to make the appropriate icon.
    """

    template = "fas fa-{icon}-circle text-{color}"

    if summary == AvailabilitySummary.GOOD:
        return template.format(icon="check", color="success")
    if summary == AvailabilitySummary.POOR:
        return template.format(icon="exclamation", color="info")
    if summary == AvailabilitySummary.NONE:
        return template.format(icon="times", color="danger")

    # AvailabilitySummary.ERROR
    return template.format(icon="question", color="warning")


def _room_filter(rooms: List[Room], search_type, search_string):
    """Finds rooms in list of rooms with the given search_type and search_string

    search_types include:

        * "startsWith": Room identifier or code begins with the given
          search_string
        * Any other value: No filtering is performed.
    """

    try:
        casefolded = search_type.casefold()
    except AttributeError:
        casefolded = ""

    if casefolded == "startswith":
        filtered = [
            room
            for room in rooms
            if (
                room.identifier.startswith(search_string)
                or room.code.startswith(search_string)
            )
        ]
    else:
        filtered = rooms

    return filtered


def route_with_complex(f):
    def wrapper(*args, **kwargs):
        return f(COMPLEX, *args, **kwargs)

    wrapper.__name__ = f.__name__

    return wrapper


def get_proper_os(possible: str) -> str:
    """Returns "Windows" or "macOS" if a similar string is passed in, "any" otherwise.

    Effectively escapes any values and ensures they are correct for passing to
    output or other Vacuity functions.

    Similarity to "Windows" or "macOS" is determined by casefolding both the
    input and the desired strings. Any possible inputs that don't match these
    are discarded and "any" is returned.
    """

    casefolded = possible.casefold()

    if casefolded == "windows":
        return "Windows"

    if casefolded == "macos":
        return "macOS"

    return "any"


@main.route("/")
@route_with_complex
def index(building_complex: Complex):
    error = request.args.get("error", default="")
    return render_template(
        "index.j2.html",
        buildings=building_complex.buildings,
        error=error,
        feedback_url=building_complex.vacuity_feedback_url,
    )


@main.route("/search")
def browse_redirect():

    try:
        building_unsafe = request.args["building"]
    except KeyError:
        return redirect("/?error=nobldg")

    building = urllib.parse.quote(building_unsafe)
    os = get_proper_os(request.args.get("os", default=""))
    software = urllib.parse.quote(request.args.get("softwareFilter", default=""))
    location = "/building/{}/rooms?operatingSystem={}&softwareFilter={}".format(
        building, os, software
    )
    return redirect(location)


@main.route("/building/<path:building>/rooms")
@route_with_complex
def browse(building_complex: Complex, building: str):

    kiosk = request.args.get("kiosk") is not None

    # Search by OS is not yet implemented
    os = "any"  # get_proper_os(request.args.get("operatingSystem", "any"))

    # Search by available software is not yet implemented
    software = ""  # escape(request.args.get("softwareFilter", ""))

    search_type = request.args.get("searchType", "")
    search_string = request.args.get("search", "")

    try:
        building = building_complex.building_for_identifier(building)
    except BuildingNotFoundError:
        abort(404)

    rooms = _room_filter(building.rooms, search_type, search_string)

    description = building.description
    if software:
        description += " with " + software
    if os != "any":
        description += " on " + os
    description += " as of {:%I:%M %p, %b %d, %Y}".format(datetime.now())

    return render_template(
        "building-rooms.j2.html",
        building=building,
        description=description,
        rooms=rooms,
        get_icon=_icon_for_availability_summary,
        feedback_url=building_complex.vacuity_feedback_url,
        kiosk=kiosk,
    )


@main.route("/building/<path:building>/rooms/<path:room>")
@route_with_complex
def lab(building_complex: Complex, building: str, room: str):
    try:
        building = building_complex.building_for_identifier(building)
    except BuildingNotFoundError:
        abort(404)

    try:
        room = building.room_for_identifier(room)
    except RoomNotFoundError:
        abort(404)

    return render_template(
        "room.j2.html",
        building=building,
        room=room,
        get_icon=_icon_for_availability_summary,
        feedback_url=building_complex.vacuity_feedback_url,
    )
