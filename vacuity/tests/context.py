# pylint: disable=wrong-import-position,unused-import
"""Allows the tests to find the Vacuity module.

To import Vacuity in test modules, use ``from .context import vacuity``.
"""
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

import vacuity
