# pylint: disable=wrong-import-order,unused-import,protected-access,missing-module-docstring
from .context import vacuity
from vacuity.client import mock
from vacuity import controllers


def test_filter_rooms_empty():
    """Ensure that filter_rooms blank or invalid filter works correctly"""

    bio_rooms = mock.MOCKED_COMPLEX.building_for_identifier("BIO").rooms

    assert controllers._room_filter(bio_rooms, "blah", "blah") == bio_rooms
    assert controllers._room_filter(bio_rooms, None, None) == bio_rooms


def test_filter_rooms_starts_with():
    """Ensure that the startsWith filter works correctly"""

    bio_rooms = mock.MOCKED_COMPLEX.building_for_identifier("BIO").rooms
    assert controllers._room_filter(bio_rooms, "startsWith", "00") == [
        mock.ROOM_BIO_002,
        mock.ROOM_BIO_003,
    ]


def test_filter_room_starts_with_accepts_case():
    """Ensure that arbitrary casing can be used to call startsWith"""

    bio_rooms = mock.MOCKED_COMPLEX.building_for_identifier("BIO").rooms
    assert controllers._room_filter(bio_rooms, "sTaRtSWiTh", "00") == [
        mock.ROOM_BIO_002,
        mock.ROOM_BIO_003,
    ]
