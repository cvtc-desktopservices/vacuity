"""Runs Vacuity with the CVTC client class

Requires the following items in the environment:

* LABSTATS_API_KEY: API key with the Stations and Applications permission
* UPSTREAM_DATA_API: A URL to the room scheduling endpoint that `Chronology`_
  was made to decode. Include the text ``{campus}`` and ``{room_number}``,
  these will be substituted with the campus and room number for each query.

Allows the following items in the environment:

* VACUITY_CVTC_CACHE_DIR: Directory to place the CacheControl file cache. If
  not provided, a CacheControl memory cache will be created. You must use a
  file cache in production, otherwise you will hit rate limits from LabStats.
  Vacuity must have ownership of this directory. It is recommended to create a
  memory-backed filesystem such as a tmpfs for this purpose.
* FEEDBACK_URL: A URL to a survey or other feedback point. This will be placed
  at the bottom of every Vacuity page as "Give Feedback".

.. _chronology: https://bitbucket.org/cvtc-desktopservices/chronology/src/master/
"""

from os import environ

from cachecontrol import CacheControlAdapter
from cachecontrol.caches.file_cache import FileCache
from cachecontrol.heuristics import ExpiresAfter
from flask import Flask
from requests import Session

import vacuity.controllers
from vacuity.client import cvtc

try:
    cache_dir = environ["VACUITY_CVTC_CACHE_DIR"]
    adapter = CacheControlAdapter(
        heuristic=ExpiresAfter(minutes=1), cache=FileCache(cache_dir)
    )
except KeyError:
    adapter = CacheControlAdapter(heuristic=ExpiresAfter(minutes=1))

session = Session()
session.mount("https://", adapter)

vacuity.controllers.COMPLEX = cvtc.CVTCComplex(
    "https://api.labstats.com", environ["LABSTATS_API_KEY"], session=session
)

# Static folder is disabled as long as we only have one blueprint
# https://stackoverflow.com/a/25804585
app = Flask(__name__, static_folder=None)

# Clean up Jinja output
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True

app.register_blueprint(vacuity.controllers.main, url_prefix="/")

if __name__ == "__main__":
    app.run()
